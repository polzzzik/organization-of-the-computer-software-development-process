#include "pch.h"
#include "CommandController.h"
#include "MathLibrary.h"

using namespace UserInput;

const int MAX_VALUE = 32768;
const int RANGE = 65535;

CommandController::CommandController(std::shared_ptr<DeviceHandler> devHandler, QObject* parent)
    : QObject(parent), deviceHandler(devHandler), 
      axisCommands(std::make_shared<std::list<JoyAxisContainer>>()), 
      buttonCommands(std::make_shared<std::list<ButtonActionContainer>>()) {}

bool CommandController::InsertAxisCommand(const JoyAxisContainer& container) {
    axisCommands->push_back(container);
    return true;
}

bool CommandController::DeleteAxisCommand(const JoyAxisContainer& container) {
    for (auto& item : *axisCommands) {
        if (item.action == container.action) {
            axisCommands->remove(item);
            return true;
        }
    }
    return false;
}

bool CommandController::ModifyAxisCommand(const JoyAxisContainer& container) {
    for (auto& item : *axisCommands) {
        if (item.action == container.action) {
            item.axisId = container.axisId;
            item.joyDescriptor = container.joyDescriptor;
            item.extMinVal = container.extMinVal;
            item.extMaxVal = container.extMaxVal;
            item.deadZoneMinVal = container.deadZoneMinVal;
            item.deadZoneMaxVal = container.deadZoneMaxVal;
            return true;
        }
    }
    return false;
}

bool CommandController::InsertButtonCommand(const ButtonActionContainer& container) {
    buttonCommands->push_back(container);
    return true;
}

bool CommandController::DeleteButtonCommand(const ButtonActionContainer& container) {
    for (auto& item : *buttonCommands) {
        if (item.action == container.action) {
            buttonCommands->remove(item);
            return true;
        }
    }
    return false;
}

bool CommandController::ModifyButtonCommand(const ButtonActionContainer& container) {
    for (auto& item : *buttonCommands) {
        if (item.action == container.action) {
            item.joyDescriptor = container.joyDescriptor;
            item.buttonId = container.buttonId;
            return true;
        }
    }
    return false;
}

std::shared_ptr<std::list<JoyAxisContainer>> CommandController::RetrieveAxisCommands() {
    return axisCommands;
}

std::shared_ptr<std::list<ButtonActionContainer>> CommandController::RetrieveButtonCommands() {
    return buttonCommands;
}

void CommandController::ValidateCommandsJoystick() {
    auto joysDescriptors = deviceHandler->GetJoysDescriptors();
    for (auto& item : *axisCommands) {
        for (auto& descriptor : joysDescriptors) {
            if (item.joyDescriptor != descriptor) {
                axisCommands->remove(item);
                break;
            }
        }
    }
    for (auto& item : *buttonCommands) {
        if (item.joyDescriptor != nullptr && !item.joyDescriptor->isEmpty()) {
            for (auto& descriptor : joysDescriptors) {
                if (item.joyDescriptor != descriptor) {
                    buttonCommands->remove(item);
                    break;
                }
            }
        }
    }
}

void CommandController::RefreshKeyButtonEvent(std::shared_ptr<KeyDescriptor> keyDescriptor) {
    for (auto& item : *buttonCommands) {
        if (item.joyDescriptor != nullptr && !item.joyDescriptor->isEmpty()) {
            if (item.buttonId != -1 && item.buttonId == keyDescriptor->key) {
                item.isPressed = keyDescriptor->isPressed;
                item.isReaded = !keyDescriptor->isPressed;
                break;
            }
        }
    }
}

void ActionController::ProcessJoyButtonEvent(SDL_JoyButtonEvent joyButtonEvent) {
    for (auto& action : *buttonActions) {
        if (action.joyDescriptor != nullptr && !action.joyDescriptor->isEmpty() && 
            action.joyDescriptor->GetIndex() == joyButtonEvent.which && action.buttonId == joyButtonEvent.button) {
            action.isPressed = joyButtonEvent.type != SDL_JOYBUTTONUP;
            action.isReaded = joyButtonEvent.type == SDL_JOYBUTTONUP;
            break;
        }
    }
}

void ActionController::ProcessJoyAxisEvent(SDL_JoyAxisEvent joyAxisEvent) {
    for (auto& action : *axisActions) {
        if (action.joyDescriptor->GetIndex() == joyAxisEvent.which && action.axisId == joyAxisEvent.axis) {
            action.value = joyAxisEvent.value;
            break;
        }
    }
}

bool ActionController::CheckActionState(ButtonAction buttonAction) {
    for (auto& action : *buttonActions) {
        if (action.action == buttonAction) {
            action.isReaded = true;
            return action.isPressed;
        }
    }
    return false;
}

double ActionController::CalculateAxisValue(AxisAction axisAction) {
    for (auto& action : *axisActions) {
        if (action.action == axisAction) {
            double calculatedValue = (action.value + MAX_VALUE) * (action.extMaxVal - action.extMinVal) / RANGE + action.extMinVal;
            return MathLibrary::DeadZone(calculatedValue, action.deadZoneMaxVal, action.deadZoneMinVal);
        }
    }
    return 0;
}
