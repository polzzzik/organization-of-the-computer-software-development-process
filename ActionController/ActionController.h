#pragma once

#include <list>
#include <memory>
#include <qobject.h>

#include "SDL/SDL.h"
#include "JoyAxisContainer/JoyAxisContainer.h"
#include "ButtonActionContainer/ButtonActionContainer.h"
#include "ButtonAction/ButtonAction.h"
#include "AxisAction/AxisAction.h"
#include "KeyEventHandler.h"
#include "DeviceHandler.h"

namespace UserInput {

class DeviceHandler;  

class CommandController : public QObject {
    Q_OBJECT

    std::shared_ptr<std::list<JoyAxisContainer>> axisCommands;
    std::shared_ptr<std::list<ButtonActionContainer>> buttonCommands;
    std::shared_ptr<DeviceHandler> deviceHandler;

public slots:
    void validateCommandsJoystick();
    void refreshKeyButtonEvent(std::shared_ptr<KeyDescriptor> keyDescriptor);
    void refreshJoyButtonEvent(SDL_JoyButtonEvent joyButtonEvent);
    void refreshJoyAxisEvent(SDL_JoyAxisEvent joyAxisEvent);

public:
    CommandController(std::shared_ptr<DeviceHandler> devHandler, QObject* parent = nullptr);

    bool insertAxisCommand(const JoyAxisContainer& container);
    bool deleteAxisCommand(const JoyAxisContainer& container);
    bool modifyAxisCommand(const JoyAxisContainer& container);
    bool insertButtonCommand(const ButtonActionContainer& container);
    bool deleteButtonCommand(const ButtonActionContainer& container);
    bool modifyButtonCommand(const ButtonActionContainer& container);

    std::shared_ptr<std::list<JoyAxisContainer>> retrieveAxisCommands();
    std::shared_ptr<std::list<ButtonActionContainer>> retrieveButtonCommands();

    bool fetchActionState(ButtonAction action);
    double fetchAxisValue(AxisAction action);
};

}  
