#pragma once
#include “Core/BaseModelUnit.h”
#include “UserInputInputs.h”
#include “UserInputOutputs.h”
#include “SDL/SDL.h”
#include “ButtonAction.h”
#include “ActionController.h”
#include “InputConfigManager.h”
#include “IinputController.h”
#include “DeviceHandler.h”

namespace UserInput {
	class UserInputControllerMU :
		public Core::BaseModelUnit, IinputController

	{
	private:
		const UserInputInputs& in;
		UserInputOutputs out;

		std::shared_ptr <DeviceHandler> deviceHandler;
		std::shared_ptr <ActionController> actController;
		std::shared_ptr <UserInput::Managers::InputConfigManager> configManager;

		

		void Initialize();

	public:

		UserInputControllerMU(const UserInputInputs& in);
		const Core::IIOInterface& GetInputInterface() const;
		const Core::IIOInterface& GetOutputInterface() const;

		void Dispose();
		void Step(double dT);
		void Reset();
		const UserInputOutputs& GetOutputs() const;
		const std::shared_ptr <UserInput::Managers::IinputConfigManager> GetInputConfigManager();
	};
}
