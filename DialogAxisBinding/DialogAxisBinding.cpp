#include "DialogAxisBinding.h"


AxisDialog::DialogAxisBinding(std::shared_ptr< UserInput::IDeviceHandler> devHandler, QWidget* parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	deviceHandler = devHandler;

	devicesModel = new DevicesItemModel();
	UpdateDevices();
	ui.SelectedDevice_comboBox->setModel(devicesModel);

	QObject::connect(deviceHandler.get(), SIGNAL(JoyDeviceRemoved()), this, SLOT(JoyDeviceRemoved()));
	QObject::connect(deviceHandler.get(), SIGNAL(JoyDeviceAdded()), this, SLOT(JoyDeviceAdded()));
	QObject::connect(deviceHandler.get(), SIGNAL(AxisMotion(SDL_JoyAxisEvent)), this, SLOT(AxisMotion(SDL_JoyAxisEvent)));

}

void AxisDialog::Init(const UserInput::JoyAxisContainer& joyAxisContainer)
{
	selectedJoyAction = joyAxisContainer;
	ui.ActionName_label->setText(QString::fromStdString(UserInput::AxisActionConv::AxisActionToStr(selectedJoyAction.action)));

	ui.ExtMin_lineEdit->setText(QString::number(selectedJoyAction.extMinVal));
	ui.ExtMax_lineEdit->setText(QString::number(selectedJoyAction.extMaxVal));
	ui.DeadZoneMin_lineEdit->setText(QString::number(selectedJoyAction.deadZoneMinVal));
	ui.DeadZoneMax_lineEdit->setText(QString::number(selectedJoyAction.deadZoneMaxVal));

	if (selectedJoyAction.joyDescriptor == nullptr || selectedJoyAction.joyDescriptor->isEmpty())
	{
		SelectJoyDescriptor(nullptr);
		ui.SelectedDevice_comboBox->setCurrentIndex(0);
	}
	else
	{
		SelectJoyDescriptor(selectedJoyAction.joyDescriptor);
		ui.SelectedDevice_comboBox->setCurrentIndex(devicesModel->GetIdByDevice(selectedJoyAction.joyDescriptor));
	}
//	UpdateAxisWidgetArea();
}

DialogAxisBinding::~DialogAxisBinding()
{
}

void DialogAxisBinding::AxisMoved(const JoyAxisTransporter& joyAxisTransporter)
{
	if (selectedJoyAction.joyDescriptor != nullptr && !selectedJoyAction.joyDescriptor->isEmpty())
	{
		if (joyAxisTransporter.descriptor == selectedJoyAction.joyDescriptor)
		{
			if (joyAxisTransporter.id >= 0)
			{
				listJoyAxis[joyAxisTransporter.id]->SetValue(joyAxisTransporter.value);
			}
		}

	}
}

void DialogAxisBinding::JoyDeviceRemoved()
{
	ui.SelectedDevice_comboBox->update();
	UpdateDevices();
	SelectJoyDescriptor(devicesModel->GetDescriptorById(ui.SelectedDevice_comboBox->currentIndex()));
}

void DialogAxisBinding::JoyDeviceAdded()
{
	UpdateDevices();
}

void DialogAxisBinding::reject()
{
	QDialog::reject();
}

void AxisControlDialog::Approve()
{
	if (chosenJoyAction.joyDescriptor == nullptr || chosenJoyAction.joyDescriptor->isEmpty()|| chosenJoyAction.axisId<0)
	{
		chosenJoyAction.joyDescriptor = nullptr;
		chosenJoyAction.axisId = -1;
	}
	emit ModifyAxisActionBinding(chosenJoyAction);
	QDialog::accept();
}

void AxisControlDialog::Link(int joyAxisId)
{
	chosenJoyAction.axisId = joyAxisId;
	RefreshAxisBindings();
}

void AxisControlDialog::ActiveDeviceChanged(int index)
{
	auto activeDevice = devicesModel->GetDescriptorById(index);
	ChooseJoyDescriptor(devicesModel->GetDescriptorById(index));
}

void AxisControlDialog::EmptyListAxis()
{
	for (auto item : joyAxisList)
	{
		item->disconnect();
	}
	joyAxisList.clear();
}

void AxisControlDialog::RefreshListAxis()
{
	EmptyListAxis();
	if (chosenJoyAction.joyDescriptor != nullptr)
	{
		int count = handler->GetAxesCount(chosenJoyAction.joyDescriptor);
		for (int i = 0; i < count; i++)
		{
			auto joyWidg = std::make_shared< JoyWidget>(i, this);
			joyAxisList.push_back(joyWidg);

			QObject::connect(joyWidg.get(), SIGNAL(Link(int)), this, SLOT(Link(int)));
		}
		RefreshAxisBindings();
	}
}

void AxisControlDialog::RefreshAxisBindings()
{
	int count = handler->GetAxesCount(chosenJoyAction.joyDescriptor);
	for (int i = 0; i < count; i++)
	{
		if (chosenJoyAction.joyDescriptor != nullptr && !chosenJoyAction.joyDescriptor->isEmpty())
		{
			if (chosenJoyAction.axisId == i)
			{
				joyAxisList[i]->Select();

			}
			else
			{
				joyAxisList[i]->Deselect();
			}
		}
	}
}


void AxisControlDialog::RefreshAxisWidgetArea()
{
//	ui.AxesScrollAreaWidgetContents->layout()->
	for (auto item : joyAxisList)
	{
		ui.AxesScrollAreaWidgetContents->layout()->addWidget(item.get());
	}
}


void AxisControlDialog::ChooseJoyDescriptor(std::shared_ptr < IJoyDescriptor> joyDescriptor)
{
	chosenJoyAction.joyDescriptor = joyDescriptor;
	if (chosenJoyAction.joyDescriptor!=nullptr)
	{
		ui.DeviceName_label->setText(QString::fromStdString(chosenJoyAction.joyDescriptor->GetName()));
	}
	else
	{
		ui.DeviceName_label->setText("");
	}
	RefreshListAxis();
	RefreshAxisWidgetArea();
}


void AxisControlDialog::RefreshDevices()
{
	devicesModel->RefreshDevices(handler->GetJoysDescriptors());
}
