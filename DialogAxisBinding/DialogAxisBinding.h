#pragma once

#include <QDialog>
#include "ui_DialogAxisBinding.h"
#include "IDeviceHandler.h"
#include "AxisAction.h"
#include "JoyWidget.h"
#include <vector>
#include "JoyAxisContainer.h"
#include "DevicesItemModel.h"
#include "JoyAxisTransporter.h"

class AxisControlDialog : public QDialog
{
	Q_OBJECT

public:
	AxisControlDialog(std::shared_ptr< UserInput::IDeviceHandler> handler, QWidget* parent = nullptr);
	void Initialize(const UserInput::JoyAxisContainer& joyAxisContainer);
	~AxisControlDialog();
	void AxisShifted(const JoyAxisTransporter& joyAxisTransporter);

signals:
	void ModifyAxisActionBinding(UserInput::JoyAxisContainer& joyAxisContainer);

public slots:
	void JoyDeviceDisconnected();
	void JoyDeviceConnected();

	void Decline();
	void Approve();

	void Link(int axisId);
	void ActiveDeviceChanged(int index);
private:
	void EmptyListAxis();
	void RefreshListAxis();
	void RefreshAxisBindings();
	void RefreshAxisWidgetArea();
	void ChooseJoyDescriptor(std::shared_ptr < IJoyDescriptor> joyDescriptor);
	void RefreshDevices();
	Ui::DialogAxisBindingClass ui;
	std::shared_ptr< UserInput::IDeviceHandler> handler;

	std::vector<std::shared_ptr<JoyWidget>> joyAxisList;
	UserInput::JoyAxisContainer chosenJoyAction;
	DevicesItemModel* devicesModel = nullptr;
};
