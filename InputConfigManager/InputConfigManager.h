#pragma once
#include "ActionController.h"
#include "AxisCfg.h"
#include <memory>
#include <vector>
#include "IinputConfigManager.h"
#include <qobject.h>
#include "IdeviceHandler.h"
#include "ButtonActionContainer.h"
#include "JoyAxisContainer.h"

namespace UserInput {
	namespace Managers
	{
		class InputConfigManager :
			public IinputConfigManager
		{

			Q_OBJECT
			Q_INTERFACES(UserInput::Managers::IinputConfigManager)

		signals:
			void AxisActionsRefreshed();
			void ButtonActionsRefreshed();

		public:
			InputConfigManager(std::shared_ptr<DeviceHandler> device_handler);
			~InputConfigManager();
			std::shared_ptr<ActionController> RetrieveActionController();

			std::shared_ptr<IdeviceHandler> FetchDeviceHandler();

			void Initialize();
			void DefaultSave();
			bool ReadFromFile(std::string file_name);
			void WriteToFile(std::string file_name);

			void InsertAxisAction(const JoyAxisContainer& axis_container);
			void DeleteAxisAction(const JoyAxisContainer& axis_container);
			void ModifyAxisAction(const JoyAxisContainer& axis_container);
			void InsertButtonAction(const ButtonActionContainer& button_container);
			void DeleteButtonAction(const ButtonActionContainer& button_container);
			void ModifyButtonAction(const ButtonActionContainer& button_container);
			void CommitChanges();

			std::shared_ptr<std::vector<double>> RetrieveAxesValues(int id);
			void RefreshAxes();
			void TerminateAxes();

			void AssignAxisDependency(AxisCfg axis_cfg);

			const std::list<std::shared_ptr<JoyDescriptor>>& FetchJoysDescriptors();

			const std::shared_ptr<std::list<JoyAxisContainer>> FetchAxisActions();
			const std::shared_ptr<std::list<ButtonActionContainer>> FetchButtonActions();

		private:
			std::shared_ptr<ActionController> action_controller;

			std::shared_ptr<DeviceHandler> device_handler;

			std::string default_cfg_filename = "BindingCfgFile";
			std::string last_filename = "";
			bool is_events_listen = false;

			bool ValidateInput(std::shared_ptr<std::list<std::string>> loaded_devices);
			std::shared_ptr<ActionController> LoadDefaultConfiguration();
			std::shared_ptr<QjsonObject> GenerateJsonOutput();
			SDL_Event sdl_event;
			SDL_Joystick* joystick;

			std::shared_ptr<std::vector<double>> current_axes_val;

		};
	}
}
