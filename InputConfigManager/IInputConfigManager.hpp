#pragma once
#include <memory>
#include <vector>
#include <string>
#include "ButtonAction.h"
#include "AxisCfg.h"
#include <list>
#include "JoyAxisContainer.h"
#include "ButtonActionContainer.h"
#include <qobject.h>
#include "IdeviceHandler.h"

#ifdef USERINPUT_EXPORTS
#define USERINPUT_API __declspec(dllexport) 
#else
#define USERINPUT_API __declspec(dllimport) 
#endif

namespace UserInput {
	namespace Managers
	{
		class USERINPUT_API IinputSettingsManager :
			public Qobject
		{

		public:
			virtual std::shared_ptr<IdeviceHandler> RetrieveDeviceHandler() = 0;
			virtual bool ImportFromFile(std::string Filename) = 0;
			virtual void ExportToFile(std::string Filename) = 0;

			virtual void InsertAxisAction(const JoyAxisContainer& container) = 0;
			virtual void DeleteAxisAction(const JoyAxisContainer& container) = 0;
			virtual void ModifyAxisAction(const JoyAxisContainer& container) = 0;
			virtual void InsertButtonAction(const ButtonActionContainer& container) = 0;
			virtual void DeleteButtonAction(const ButtonActionContainer& container) = 0;
			virtual void ModifyButtonAction(const ButtonActionContainer& container) = 0;
			virtual void ImplementChanges() = 0;

			//	signals:
			virtual void ButtonActionsRefreshed() = 0;
			virtual void AxisActionsRefreshed() = 0;


			virtual const std::list<std::shared_ptr< JoyDescriptor>>& RetrieveJoysDescriptors() = 0;

			virtual std::shared_ptr < std::vector<double>> RetrieveAxesValues(int id) = 0;
			virtual void RefreshAxes() = 0;
			virtual void DisconnectAxes() = 0;

			virtual void AssignAxisDependency(AxisCfg axisCfg) = 0;

			virtual const std::shared_ptr<std::list < JoyAxisContainer>> RetrieveAxisActions() = 0;
			virtual const std::shared_ptr<std::list < ButtonActionContainer>> RetrieveButtonActions() = 0;

		};
	}
}

Q_DECLARE_INTERFACE(UserInput::Managers:: IinputSettingsManager, "QinputLibrary")
