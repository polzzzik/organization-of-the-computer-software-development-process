#include "pch.h"
#include "InputSettingsManager.h"
#include <fstream>
#include <Qfile>
#include <Qdir>
#include <QjsonObject>
#include <QjsonArray>
#include <QjsonDocument>
#include <thread>

using namespace UserInput;
using namespace UserInput::Managers;

bool UserInput::Managers::InputSettingsManager::VerifyInput(std::shared_ptr < std::list < std::string>> loadedDevices)
{
	std::list<std::string> tmp;
	auto joyDescriptors = deviceHandler->GetJoysDescriptors();
	for (auto it : joyDescriptors)
	{
		tmp.push_back(it->GetName());
	}
	return *loadedDevices.get() == tmp;
}

std::shared_ptr<ActionController> UserInput::Managers::InputSettingsManager::LoadStandardConfig()
{
	auto controller = std::make_shared<ActionController>(deviceHandler);

	for (int i = 0; i < ButtonActionConv::size(); i++)
	{
		ButtonActionContainer container;
		container.action = ButtonActionConv::At(i);
		controller->AddButtonAction(container);
	}

	for (int i = 0; i < AxisActionConv::size(); i++)
	{
		JoyAxisContainer container;
		container.action = AxisActionConv::At(i);
		controller->AddAxisAction(container);
	}
	return controller;
}
UserInput::Managers::InputSettingsManager::InputSettingsManager(std::shared_ptr<DeviceHandler> devHandler)
{
	deviceHandler = devHandler;
	Load();
}

UserInput::Managers::InputSettingsManager::~InputSettingsManager()
{

}

std::shared_ptr<ActionController> UserInput::Managers::InputSettingsManager::GetConstructedActionController()
{
	Load();
	return actionController;
}

std::shared_ptr<IdeviceHandler> UserInput::Managers::InputSettingsManager::RetrieveDeviceHandler()
{
	return deviceHandler;
}

void UserInput::Managers::InputSettingsManager::Load()
{
	Qstring val;
	Qfile file;
	file.setFileName(Qstring::fromStdString(StdCfgFilename));
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QjsonParseError error;
	QjsonDocument doc = QjsonDocument::fromJson(val.toUtf8(), &error);

	std::string filepath;
	if (doc.isObject())
	{
		QjsonObject json = doc.object();
		filepath = (json["LastConfigPath"].toString()).toStdString();
	}

	bool isLoaded = false;
	if (filepath != "")
	{
		isLoaded = ImportFromFile(filepath);
	}
	else
	{
		isLoaded = ImportFromFile(StdCfgFilename);
	}

	if (!isLoaded)
	{
		actionController = LoadStandardConfig();
	}
}

void UserInput::Managers::InputSettingsManager::StdSave()
{
	auto json = CreateJsonOutput();

	json->insert("LastConfigPath", Qstring::fromStdString(LastFilename));

	QjsonDocument doc(*json.get());
	Qstring jsonString = doc.toJson(QjsonDocument::Indented);
	Qfile file;
	file.setFileName(Qstring::fromStdString(StdCfgFilename));
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QtextStream stream(&file);
	stream << jsonString;
	file.close();
}

bool UserInput::Managers::InputConfigManager::ReadFromFile(std::string file_name)
{
    bool success = true;
    std::shared_ptr<ActionController> action_controller = std::make_shared<ActionController>(deviceHandler);

    Qstring file_content;
    Qfile input_file;
    input_file.setFileName(Qstring::fromStdString(file_name));
    input_file.open(QIODevice::ReadOnly | QIODevice::Text);
    file_content = input_file.readAll();
    input_file.close();

    QjsonParseError json_error;
    QjsonDocument json_doc = QjsonDocument::fromJson(file_content.toUtf8(), &json_error);

    auto joystick_names = std::make_shared<std::list<std::string>>();
    if (json_doc.isObject())
    {
        QjsonObject json_obj = json_doc.object();
        QjsonArray json_array;

        Qlist<Qvariant> joystick_list = json_obj["Joystics"].toArray().toVariantList();

        foreach(const Qvariant & joystick, joystick_list)
        {
            std::string joystick_name;
            joystick_name = joystick.toString().toStdString();
            joystick_names->push_back(joystick_name);
        }

        success = ValidateInput(joystick_names);
        json_array = json_obj["axisActions"].toArray();

        foreach(const QjsonValue & axis_action, json_array)
        {
            if (axis_action.isObject())
            {
                JoyAxisContainer joy_axis;

                QjsonObject obj = axis_action.toObject();
                joy_axis.action = static_cast<UserInput::AxisAction>(obj["action"].toInt());
                joy_axis.axisId = obj["axisId"].toInt();
                joy_axis.value = obj["value"].toDouble();
                joy_axis.minVal = obj["minVal"].toDouble();
                joy_axis.maxVal = obj["maxVal"].toDouble();
                joy_axis.extMinVal = obj["extMinVal"].toDouble();
                joy_axis.extMaxVal = obj["extMaxVal"].toDouble();
                joy_axis.deadZoneMinVal = obj["deadZoneMinVal"].toDouble();
                joy_axis.deadZoneMaxVal = obj["deadZoneMaxVal"].toDouble();

                auto descriptor = deviceHandler->GetDescriptorById(obj["joyId"].toInt());
                if (descriptor != nullptr)
                {
                    if (obj["joyName"].toString().toStdString() == descriptor->GetName())
                    {
                        joy_axis.joyDescriptor = descriptor;
                    }
                }
                action_controller->AddAxisAction(joy_axis);
            }
        }

        json_array = json_obj["buttonActions"].toArray();
        foreach(const QjsonValue & button_action, json_array)
        {
            if (button_action.isObject())
            {
                ButtonActionContainer button_container;

                QjsonObject obj = button_action.toObject();
                button_container.action = static_cast<UserInput::ButtonAction>(obj["action"].toInt());
                button_container.isReaded = obj["isReaded"].toBool();
                button_container.buttonId = obj["buttonId"].toInt();
                button_container.isPressed = obj["isPressed"].toBool();

                auto descriptor = deviceHandler->GetDescriptorById(obj["joyId"].toInt());
                if (descriptor != nullptr)
                {
                    if (obj["joyName"].toString().toStdString() == descriptor->GetName())
                    {
                        button_container.joyDescriptor = descriptor;
                    }
                }

                action_controller->AddButtonAction(button_container);
            }
        }
        if (action_controller->GetAxisActions()->size() != AxisActionConv::size() ||
            action_controller->GetButtonActions()->size() != ButtonActionConv::size())
        {
            success = false;
        }
    }

    if (success)
    {
        actionController = action_controller;
    }

    return success;
}

void UserInput::Managers::InputConfigManager::WriteToFile(std::string file_name)
{
    auto json_output = GenerateJsonOutput();

    QjsonDocument json_doc(*json_output.get());
    Qstring json_string = json_doc.toJson(QjsonDocument::Indented);
    Qfile output_file;
    output_file.setFileName(Qstring::fromStdString(file_name));
    output_file.open(QIODevice::WriteOnly | QIODevice::Text);
    QtextStream stream(&output_file);
    stream << json_string;
    output_file.close();
}

std::shared_ptr<QjsonObject> UserInput::Managers::InputConfigManager::GenerateJsonOutput()
{
    std::shared_ptr<QjsonObject> json_obj = std::make_shared<QjsonObject>();
    QjsonArray json_array;

    auto joystick_descriptors = deviceHandler->GetJoysDescriptors();
    for (auto descriptor : joystick_descriptors)
    {
        json_array.append(Qstring::fromStdString(descriptor->GetName()));
    }
    json_obj->insert("Joystics", json_array);

    json_array = QjsonArray();
    auto& axis_actions = *actionController->GetAxisActions();
    for (auto& action : axis_actions)
    {
        QjsonObject temp_obj;
        temp_obj.insert("action", QjsonValue::fromVariant((int)action.action));
        temp_obj.insert("axisId", QjsonValue::fromVariant(action.axisId));
        if (action.joyDescriptor != nullptr && !action.joyDescriptor->isEmpty())
        {
            temp_obj.insert("joyId", QjsonValue::fromVariant(action.joyDescriptor->GetIndex()));
            temp_obj.insert("joyName", Qstring::fromStdString(action.joyDescriptor->GetName()));
        }

        temp_obj.insert("minVal", QjsonValue::fromVariant(action.minVal));
        temp_obj.insert("maxVal", QjsonValue::fromVariant(action.maxVal));
        temp_obj.insert("extMinVal", QjsonValue::fromVariant(action.extMinVal));
        temp_obj.insert("extMaxVal", QjsonValue::fromVariant(action.extMaxVal));
        temp_obj.insert("deadZoneMinVal", QjsonValue::fromVariant(action.deadZoneMinVal));
        temp_obj.insert("deadZoneMaxVal", QjsonValue::fromVariant(action.deadZoneMaxVal));

        json_array.append(temp_obj);
    }
    json_obj->insert("axisActions", json_array);

    json_array = QjsonArray();
    auto& button_actions = *actionController->GetButtonActions();
    for (auto& action : button_actions)
    {
        QjsonObject temp_obj;
        temp_obj.insert("action", QjsonValue::fromVariant((int)action.action));
        temp_obj.insert("buttonId", QjsonValue::fromVariant(action.buttonId));
        if (action.joyDescriptor != nullptr && !action.joyDescriptor->isEmpty())
        {
            temp_obj.insert("joyId", QjsonValue::fromVariant(action.joyDescriptor->GetIndex()));
            temp_obj.insert("joyName", Qstring::fromStdString(action.joyDescriptor->GetName()));
        }
        json_array.append(temp_obj);
    }
    json_obj->insert("buttonActions", json_array);

    return json_obj;
}

void UserInput::Managers::InputConfigManager::InsertAxisAction(const JoyAxisContainer& joy_axis)
{
    if (actionController->AddAxisAction(joy_axis))
    {
        emit UpdatedAxisActions();
    }
}
void UserInput::Managers::InputConfigManager::DeleteAxisAction(const JoyAxisContainer& axis_container)
{
    if (actionController->RemoveAxisAction(axis_container))
    {
        emit AxisActionsRefreshed();
    }
}

void UserInput::Managers::InputConfigManager::ModifyAxisAction(const JoyAxisContainer& axis_container)
{
    if (actionController->UpdateAxisAction(axis_container))
    {
        emit AxisActionsRefreshed();
    }
}

void UserInput::Managers::InputConfigManager::InsertButtonAction(const ButtonActionContainer& button_container)
{
    if (actionController->AddButtonAction(button_container))
    {
        emit ButtonActionsRefreshed();
    }
}

void UserInput::Managers::InputConfigManager::DeleteButtonAction(const ButtonActionContainer& button_container)
{
    if (actionController->RemoveButtonAction(button_container))
    {
        emit ButtonActionsRefreshed();
    }
}

void UserInput::Managers::InputConfigManager::ModifyButtonAction(const ButtonActionContainer& button_container)
{
    if (actionController->UpdateButtonAction(button_container))
    {
        emit ButtonActionsRefreshed();
    }
}

void UserInput::Managers::InputConfigManager::CommitChanges()
{
    DefaultSave();
}

const std::shared_ptr<std::list<JoyAxisContainer>> UserInput::Managers::InputConfigManager::FetchAxisActions()
{
    return actionController->GetAxisActions();
}

const std::shared_ptr<std::list<ButtonActionContainer>> UserInput::Managers::InputConfigManager::FetchButtonActions()
{
    return actionController->GetButtonActions();
}
