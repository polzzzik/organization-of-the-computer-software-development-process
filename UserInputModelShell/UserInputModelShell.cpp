#include "pch.h"
#include "UserInputModelShell.h"
#include "UserInputControllerMU.h"

namespace UserInput {

UserInputModelShell::UserInputModelShell(const UserInputInputs& in)
	: Core::BaseModelShell(std::make_shared<UserInputControllerMU>(in))
{
}

const UserInputOutputs& UserInputModelShell::GetOutputs()
{
	auto controller = static_cast<UserInputControllerMU*>(currentModel.get());
	return controller->GetOutputs();
}

std::shared_ptr<Managers::IinputConfigManager> UserInputModelShell::GetInputConfigManager()
{
	auto controller = static_cast<UserInputControllerMU*>(currentModel.get());
	return controller->GetInputConfigManager();
}

}