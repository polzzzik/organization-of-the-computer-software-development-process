#pragma once

#ifdef USERINPUT_EXPORTS
#define USERINPUT_API __declspec(dllexport) 
#else
#define USERINPUT_API __declspec(dllimport) 
#endif

#include "BaseModelShell.h"
#include "UserInputInputs.h"
#include "UserInputOutputs.h"
#include "IInputController.h"

namespace UserInput {

	class UserInputModelShell : public Core::BaseModelShell, public IinputController
	{
	public:
		USERINPUT_API UserInputModelShell(const UserInputInputs& in);
		USERINPUT_API ~UserInputModelShell();

		USERINPUT_API const UserInputOutputs& FetchOutputs();
		USERINPUT_API std::shared_ptr<Managers::IinputConfigManager> RetrieveInputConfigManager();
	};

} // namespace UserInput
