
#ifndef DEVICE_HANDLER_H
#define DEVICE_HANDLER_H

#include <list>
#include <memory>
#include <vector>
#include <qobject.h>
#include "SDL/SDL.h"
#include "JoyDescriptor.h"
#include "KeyEventHandler.h"
#include "IDeviceHandler.h"

namespace UserInput {


class DeviceHandler : public IDeviceHandler {
  Q_OBJECT
  Q_INTERFACES(UserInput::IDeviceHandler)

public:
  DeviceHandler(QObject *parent = nullptr);
  ~DeviceHandler();

  const std::list<std::shared_ptr<IJoyDescriptor>> &GetJoysDescriptors();
  const std::shared_ptr<IJoyDescriptor> GetDescriptorById(int id);
  void StartListenEvents();
  int GetAxesCount(std::shared_ptr<IJoyDescriptor> joy);

signals:
  void JoyDeviceAdded();
  void JoyDeviceRemoved();
  void JoyButtonDown(SDL_JoyButtonEvent joyButtonEvent);
  void JoyButtonUp(SDL_JoyButtonEvent joyButtonEvent);
  void KeyButtonEvent(std::shared_ptr<KeyDescriptor> keyDescriptor);
  void AxisMotion(SDL_JoyAxisEvent joyAxisEvent);

private:
  std::list<std::shared_ptr<JoyDescriptor>> joysDescriptors; 
  std::list<std::shared_ptr<IJoyDescriptor>> IJoysDescriptors;
  bool isEventsListen; 
  SDL_Event SDLevent;
  KeyEventHandler keyEventHandler; 
  
  void AddJoystic(Sint32 which); 
  void RemoveJoystic(Sint32 which); 
  void UpdateEvents(); 
  void EventHandler(); 
};

} 

#endif 
