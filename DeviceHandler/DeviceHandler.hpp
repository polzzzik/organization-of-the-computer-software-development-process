#pragma once

#include <qobject.h>
#include <list>
#include "SDL/SDL.h"
#include "IJoyDescriptor.h"
#include "KeyEventHandler.h"
#define USERINPUT_API __declspec(dllimport) 
#endif

namespace UserInput {

    class USERINPUT_API IDeviceManager : public QObject {
    public:
        virtual ~IDeviceManager() = default;

        virtual const std::list<std::shared_ptr<IJoyDescriptor>>& FetchJoysDescriptors() = 0;
        virtual void InitiateEventListening() = 0;
        virtual int RetrieveAxesCount(std::shared_ptr<IJoyDescriptor> joy) = 0;
        virtual const std::shared_ptr<IJoyDescriptor> FetchDescriptorById(int id) = 0;

        
        virtual void JoystickDeviceRemoved() = 0;
        virtual void JoystickDeviceAdded() = 0;
        virtual void JoystickButtonDown(SDL_JoyButtonEvent joyButtonEvent) = 0;
        virtual void JoystickButtonUp(SDL_JoyButtonEvent joyButtonEvent) = 0;
        virtual void KeyPressEvent(std::shared_ptr<KeyDescriptor> keyDescriptor) = 0;
        virtual void AxisMovement(SDL_JoyAxisEvent joyAxisEvent) = 0;
    };

}

Q_DECLARE_INTERFACE(UserInput::IDeviceManager, "QInputLibrary")

#pragma once

#include <qobject.h>
#include <list>
#include <memory>
#include <vector>
#include "SDL/SDL.h"
#include "JoyDescriptor.h"
#include "KeyEventHandler.h"
#include "IDeviceManager.h"

namespace UserInput {

    class DeviceManager : public IDeviceManager {
        Q_OBJECT
        Q_INTERFACES(UserInput::IDeviceManager)

    public:
        explicit DeviceManager(QObject* parent = nullptr);
        ~DeviceManager() override;

        const std::list<std::shared_ptr<IJoyDescriptor>>& FetchJoysDescriptors() override;
        const std::shared_ptr<IJoyDescriptor> FetchDescriptorById(int id) override;
        void InitiateEventListening() override;
        int RetrieveAxesCount(std::shared_ptr<IJoyDescriptor> joy) override;

    signals:
        void JoystickDeviceRemoved() override;
        void JoystickDeviceAdded() override;
        void JoystickButtonDown(SDL_JoyButtonEvent joyButtonEvent) override;
        void JoystickButtonUp(SDL_JoyButtonEvent joyButtonEvent) override;
        void KeyPressEvent(std::shared_ptr<KeyDescriptor> keyDescriptor) override;
        void AxisMovement(SDL_JoyAxisEvent joyAxisEvent) override;

    private:
        std::list<std::shared_ptr<JoyDescriptor>> joysDescriptorsList;
        std::list<std::shared_ptr<IJoyDescriptor>> IJoysDescriptorsList;

        bool isListeningEvents = false;
        SDL_Event SDLeventInstance;
        KeyEventHandler keyEventHandlerInstance;

        void AddJoystick(Sint32 which);
        void RemoveJoystick(Sint32 which);
        void RefreshEvents();
        void HandleEvent();
    };

}
