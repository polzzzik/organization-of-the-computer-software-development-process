#ifndef IDEVICE_HANDLER_H
#define IDEVICE_HANDLER_H

#include <list>
#include <memory>
#include <qobject.h>
#include "SDL/SDL.h"
#include "JoyDescriptor/IJoyDescriptor.h"
#include "KeyEventHandler.h"

#ifdef USERINPUT_EXPORTS
    #define USERINPUT_API __declspec(dllexport) 
#else
    #define USERINPUT_API __declspec(dllimport) 
#endif

namespace UserInput {

class USERINPUT_API IDeviceHandler : public QObject {
public:
  
  virtual ~IDeviceHandler() = default;
  
  virtual const std::list<std::shared_ptr<IJoyDescriptor>> &GetJoysDescriptors() = 0;
  virtual void StartListenEvents() = 0;
  virtual int GetAxesCount(std::shared_ptr<IJoyDescriptor> joy) = 0;
  virtual const std::shared_ptr<IJoyDescriptor> GetDescriptorById(int id) = 0;
  virtual void JoyDeviceRemoved() = 0;
  virtual void JoyDeviceAdded() = 0;
  virtual void JoyButtonDown(SDL_JoyButtonEvent joyButtonEvent) = 0;
  virtual void JoyButtonUp(SDL_JoyButtonEvent joyButtonEvent) = 0;
  virtual void KeyButtonEvent(std::shared_ptr<KeyDescriptor> keyDescriptor) = 0;
  virtual void AxisMotion(SDL_JoyAxisEvent joyAxisEvent) = 0;
};

}

Q_DECLARE_INTERFACE(UserInput::IDeviceHandler, "QInputLibrary")
#endif 
