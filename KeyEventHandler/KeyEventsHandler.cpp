// Реализация класса обработчика клавиш
#include "KeyInputHandler.h"

void KeyInputHandler::InspectKeyStates()
{
    for (int i = 0; i < 255; i++)
    {
        bool currentKeyState = GetKeyState(i);
        if (currentKeyState != previousKeyStates[i])
        {
            previousKeyStates[i] = currentKeyState;
            auto keyInfo = std::make_shared<KeyInfo>();
            keyInfo->isPressed = currentKeyState;
            keyInfo->keyCode = i;
            keyEvents.push_back(keyInfo);
        }
    }
}

std::shared_ptr<KeyInfo> KeyInputHandler::RetrieveKeyEvent()
{
    std::shared_ptr<KeyInfo> keyEvent = nullptr;
    if (!keyEvents.empty())
    {
        keyEvent = keyEvents.back();
        keyEvents.pop_back();
    }
    return keyEvent;
}
