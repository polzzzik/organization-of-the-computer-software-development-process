#pragma once
#include <Windows.h>
#include <vector>
#include <memory>

class KeyInfo
{
public:
	bool keyStatus = false;
	int keyCode = -1;
};

class KeyboardEventHandler
{
private:
	bool keyStates[255];
	std::vector<std::shared_ptr<KeyInfo>> keyEvents;

public:
	void VerifyState();
	std::shared_ptr<KeyInfo> RetrieveEvent();
};
