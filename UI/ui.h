#pragma once
#include <QtWidgets/QMainWindow>
#include <memory>
#include <QStringListModel>
#include <qstandarditemmodel.h>
#include "SDL/SDL.h"
#include "AxisAction.h"
#include "IJoystickDescriptor.h"
#include "ButtonAction.h"
#include "DialogWaitForKey.h"
#include "IDeviceManager.h"
#include "JoystickAxisContainer.h"
#include "ButtonActionContainer.h"
#include "IUserInputController.h"
#include "IUserInputConfigManager.h"
#include "UserInputEvents.h"
#include "ui_UserInterface.h"

#include "JoystickWidget.h"
#include "DialogAxisMapping.h"
#include "ButtonActionDataModel.h"
#include "AxisActionDataModel.h"
#include "JoystickAxisTransporter.h"


class UserInterface : public QMainWindow
{
	Q_OBJECT
public:
	UserInterface(QWidget* parent = nullptr);

	~UserInterface();

public slots:
	void onKeyActionTableSelection(const QModelIndex& index);
	void onAxisActionTableSelection(const QModelIndex& index);

	void JoystickDeviceDisconnected();
	void JoystickDeviceConnected();
	void JoystickButtonDown(SDL_JoyButtonEvent joystickButtonEvent);
	void JoystickButtonUp(SDL_JoyButtonEvent joystickButtonEvent);
	void KeyButtonEvent(std::shared_ptr< KeyDescriptor> keyDescriptor);
	void AxisMovement(SDL_JoyAxisEvent joystickAxisEvent);

	void UpdateButtonActions();
	void UpdateAxisActions();

	void RefreshAxisActionMapping(UserInput::JoystickAxisContainer& joystickAxisContainer);

	void SaveChanges();

private:
	Ui::UserInterfaceClass ui;

	std::shared_ptr< UserInput::IUserInputController> userInputController;
	std::shared_ptr< UserInput::IDeviceManager> deviceManager;

	QStringListModel* deviceListModel = nullptr;

	std::shared_ptr< UserInput::Managers::IUserInputConfigManager> userInputConfigManager;

	void InitializeDevicesView();
	void RefreshDevicesList();
	void InitializeButtonActionView();
	void InitializeAxisActionView();
	void InitializeButtonActionsDataModel();
	void InitializeAxisActionsDataModel();

	std::list<std::shared_ptr< IJoystickDescriptor>> joystickDescriptorMemory;

	ButtonActionDataModel* buttonActionDataModel = nullptr;
	AxisActionDataModel* axisActionDataModel = nullptr;

	DialogWaitForKey* waitForKeyDialog = nullptr;
	DialogAxisMapping* axisMappingDialog = nullptr;

	bool isWaitingForKey = false;
	std::shared_ptr< UserInput::ButtonActionContainer> currentButtonAction;
	std::shared_ptr< UserInput::JoystickAxisContainer> currentAxisAction;

	UserInput::UserInputEvents userInputEvents;
};
