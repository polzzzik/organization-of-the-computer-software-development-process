#include "UserInterface.h"
#include "UserInputModelWrapper.h"
#include <thread>

using namespace UserInput;

UserInterface::UserInterface(QWidget* parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	auto temp = std::make_shared<UserInput::UserInputModelWrapper>(in);
	userInputController = std::static_pointer_cast<UserInput::IUserInputController>(temp);
	userInputConfigManager = userInputController->GetInputConfigManager();
	deviceManager = userInputConfigManager->GetDeviceManager();

	SetupDevicesView();
	SetupButtonActionView();
	SetupAxisActionView();

	waitForKeyDialog = new DialogWaitForKey(this);
	waitForKeyDialog->setModal(true);
	axisMappingDialog = new DialogAxisMapping(deviceManager, this);
	axisMappingDialog->setModal(true);

	QObject::connect(deviceManager.get(), SIGNAL(JoystickDeviceDisconnected()), this, SLOT(JoystickDeviceDisconnected()));
	QObject::connect(deviceManager.get(), SIGNAL(JoystickDeviceConnected()), this, SLOT(JoystickDeviceConnected()));
	QObject::connect(deviceManager.get(), SIGNAL(JoystickButtonDown(SDL_JoyButtonEvent)), this, SLOT(JoystickButtonDown(SDL_JoyButtonEvent)));
	QObject::connect(deviceManager.get(), SIGNAL(JoystickButtonUp(SDL_JoyButtonEvent)), this, SLOT(JoystickButtonUp(SDL_JoyButtonEvent)));
	QObject::connect(deviceManager.get(), SIGNAL(KeyButtonEvent(std::shared_ptr<KeyDescriptor>)), this, SLOT(KeyButtonEvent(std::shared_ptr<KeyDescriptor>)));
	QObject::connect(deviceManager.get(), SIGNAL(AxisMovement(SDL_JoyAxisEvent)), this, SLOT(AxisMovement(SDL_JoyAxisEvent)));

	QObject::connect(userInputConfigManager.get(), SIGNAL(ButtonActionsRefreshed()), this, SLOT(RefreshButtonActions()));
	QObject::connect(userInputConfigManager.get(), SIGNAL(AxisActionsRefreshed()), this, SLOT(RefreshAxisActions()));

	QObject::connect(axisMappingDialog, SIGNAL(RefreshAxisActionMapping(UserInput::JoystickAxisContainer&)), this, SLOT(RefreshAxisActionMapping(UserInput::JoystickAxisContainer&)));
}

void UserInterface::SetupDevicesView()
{
	deviceListModel = new QStringListModel();
	ui.ListDevices_listView->setModel(deviceListModel);
	RefreshDevicesList();
}

void UserInterface::SetupButtonActionView()
{
	SetupButtonActionsDataModel();
	ui.Key_tableView->setModel(buttonActionDataModel);
	ui.Key_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void UserInterface::SetupAxisActionView()
{
	SetupAxisActionsDataModel();
	ui.Axis_tableView->setModel(axisActionDataModel);
	ui.Axis_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

UserInterface::~UserInterface()
{
	if (deviceListModel != nullptr)
		delete deviceListModel;
	if (waitForKeyDialog != nullptr)
		delete waitForKeyDialog;
}

void UserInterface::JoystickDeviceDisconnected()
{
	RefreshDevicesList();
	axisMappingDialog->JoystickDeviceDisconnected();
}

void UserInterface::JoystickDeviceConnected()
{
	RefreshDevicesList();
	axisMappingDialog->JoystickDeviceConnected();
}

void UserInterface::JoystickButtonDown(SDL_JoyButtonEvent joystickButtonEvent)
{
	if (isWaitingForKey && currentButtonAction != nullptr)
	{
		currentButtonAction->isPressed = false;
		currentButtonAction->isReaded = true;
		currentButtonAction->joystickDescriptor = deviceManager->GetDescriptorById(joystickButtonEvent.which);
		currentButtonAction->buttonId = joystickButtonEvent.button;

		userInputConfigManager->UpdateButtonAction(*currentButtonAction.get());
		isWaitingForKey = false;
		currentButtonAction = nullptr;
		waitForKeyDialog->hide();
	}
}

void UserInterface::JoystickButtonUp(SDL_JoyButtonEvent joystickButtonEvent)
{
}

void UserInterface::KeyButtonEvent(std::shared_ptr<KeyDescriptor> keyDescriptor)
{
	if (isWaitingForKey && currentButtonAction != nullptr)
	{
		if (keyDescriptor->isPressed)
		{
			currentButtonAction->isPressed = false;
			currentButtonAction->isReaded = true;
			currentButtonAction->buttonId = keyDescriptor->key;
			currentButtonAction->joystickDescriptor = nullptr;
			userInputConfigManager->UpdateButtonAction(*currentButtonAction.get());
			isWaitingForKey = false;
			currentButtonAction = nullptr;
			waitForKeyDialog->hide();
		}
	}
}

void UserInterface::AxisMovement(SDL_JoyAxisEvent joystickAxisEvent)
{
	JoystickAxisTransporter transporter;
	transporter.descriptor = deviceManager->GetDescriptorById(joystickAxisEvent.which);
	transporter.id = joystickAxisEvent.axis;
	transporter.value = joystickAxisEvent.value;
	axisMappingDialog->AxisMoved(transporter);
}

void UserInterface::RefreshButtonActions()
{
	auto& buttonActions = *userInputConfigManager->GetButtonActions();
	for (auto item : buttonActions)
	{
		if (item.action != ButtonAction::BA_NONE)
		{
			buttonActionDataModel->RefreshButtonAction(item);
		}
	}
}

void UserInterface::RefreshAxisActions()
{
	auto& axisActions = *userInputConfigManager->GetAxisActions();
	for (auto item : axisActions)
	{
		if (item.action != AxisAction::AA_NONE)
		{
			axisActionDataModel->RefreshAxisAction(item);
		}
	}
}

void UserInterface::RefreshAxisActionMapping(UserInput::JoystickAxisContainer& joystickAxisContainer)
{
	auto& axisActions = *userInputConfigManager->GetAxisActions();
	for (auto item : axisActions)
	{
		if (item.action == joystickAxisContainer.action)
		{
			userInputConfigManager->UpdateAxisAction(joystickAxisContainer);
		}
	}
}

void UserInterface::SaveChanges()
{
	userInputConfigManager->SaveChanges();
}

void UserInterface::onAxisActionTableSelection(const QModelIndex& index)
{
	currentAxisAction = std::make_shared<UserInput::JoystickAxisContainer>(axisActionDataModel->GetActionByIndex(index.row()));

	axisMappingDialog->Initialize(*currentAxisAction.get());
	axisMappingDialog->show();
}

void UserInterface::onKeyActionTableSelection(const QModelIndex& index)
{
	isWaitingForKey = true;
	currentButtonAction = std::make_shared<UserInput::ButtonActionContainer>(buttonActionDataModel->GetActionByIndex(index.row()));
	waitForKeyDialog->show();
}

void UserInterface::RefreshDevicesList()
{
	auto joystickDescriptors = deviceManager->GetJoystickDescriptors();
	if (!(joystickDescriptorMemory == joystickDescriptors))
	{
		QString str("%1 %2");
		joystickDescriptorMemory.clear();
		deviceListModel->removeRows(0, deviceListModel->rowCount());

		deviceListModel->insertRow(deviceListModel->rowCount());
		deviceListModel->setData(deviceListModel->index(deviceListModel->rowCount() - 1), "Index\tName");
		for (auto it : joystickDescriptors)
		{
			joystickDescriptorMemory.push_back(it);
			deviceListModel->insertRow(deviceListModel->rowCount());
			QModelIndex index = deviceListModel->index(deviceListModel->rowCount() - 1);
			deviceListModel->setData(index, str.arg(it->GetIndex()).arg(QString::fromStdString(it->GetName())));
		}
		ui.ListDevices_listView->update();
	}
}

void UserInterface::SetupButtonActionsDataModel()
{
	buttonActionDataModel = new ButtonActionDataModel();
	auto& buttonActions = *userInputConfigManager->GetButtonActions();
	for (auto item : buttonActions)
	{
		if (item.action != ButtonAction::BA_NONE)
		{
			buttonActionDataModel->AddButtonAction(item);
		}
	}
}

void UserInterface::SetupAxisActionsDataModel()
{
	axisActionDataModel = new AxisActionDataModel();
	auto& axisActions = *userInputConfigManager->GetAxisActions();
	for (auto item : axisActions)
	{
		if (item.action != AxisAction::AA_NONE)
		{
			axisActionDataModel->AddAxisAction(item);
		}
	}
}
