#ifndef BUTTON_ACTION_CONTAINER_H
#define BUTTON_ACTION_CONTAINER_H

#include <memory>
#include <string>
#include "ButtonAction.h"
#include "IJoyDescriptor.h"

namespace UserInput {

class ButtonActionContainer {
public:
  ButtonActionContainer() : action(ButtonAction::BA_NONE), isReaded(true),
                            buttonId(-1), isPressed(false) {}
  ButtonActionContainer(ButtonAction act, bool read, int id,
                        std::shared_ptr<IJoyDescriptor> joy, bool press)
      : action(act), isReaded(read), buttonId(id), joyDescriptor(joy),
        isPressed(press) {}

  ButtonAction getAction() const { return action; }
  void setAction(ButtonAction act) { action = act; }

  bool getIsReaded() const { return isReaded; }
  void setIsReaded(bool read) { isReaded = read; }

  int getButtonId() const { return buttonId; }
  void setButtonId(int id) { buttonId = id; }

  std::shared_ptr<IJoyDescriptor> getJoyDescriptor() const {
    return joyDescriptor;
  }
  void setJoyDescriptor(std::shared_ptr<IJoyDescriptor> joy) {
    joyDescriptor = joy;
  }

  bool getIsPressed() const { return isPressed; }
  void setIsPressed(bool press) { isPressed = press; }

 
  bool operator==(const ButtonActionContainer &other) const {
    return action == other.action && isReaded == other.isReaded &&
           buttonId == other.buttonId && joyDescriptor == other.joyDescriptor &&
           isPressed == other.isPressed;
  }


  ButtonActionContainer &operator=(const ButtonActionContainer &other) {
    if (this != &other) {
      action = other.action;
      isReaded = other.isReaded;
      buttonId = other.buttonId;
      joyDescriptor = other.joyDescriptor;
      isPressed = other.isPressed;
    }
    return *this;
  }

private:
  
  ButtonAction action; 
  bool isReaded;       
  int buttonId;        
  std::shared_ptr<IJoyDescriptor> joyDescriptor; 
  bool isPressed; 
};

} 

#endif BUTTON_ACTION_CONTAINER_H
