#pragma once

#include "AxisAction.h"
#include "SDL/SDL.h"
#include <string>
#include "IJoyDescriptor.h"
#include <memory>

namespace UserInput
{
	class JoyAxisWrapper
	{
	public:
		AxisAction actionType = AxisAction::AA_NONE;
		int axisIdentifier = -1;
		std::shared_ptr< IJoyDescriptor> joyDesc = nullptr;
		double currentValue = 0;
		double minValue = -32768;
		double maxValue = 32767;
		double externalMinValue = -32768;
		double externalMaxValue = 32767;
		double deadZoneMinValue = 0;
		double deadZoneMaxValue = 0;

		JoyAxisWrapper& operator =(const JoyAxisWrapper& p)
		{
			actionType = p.actionType;
			axisIdentifier = p.axisIdentifier;
			joyDesc = p.joyDesc;
			currentValue = p.currentValue;
			minValue = p.minValue;
			maxValue = p.maxValue;
			externalMinValue = p.externalMinValue;
			externalMaxValue = p.externalMaxValue;
			deadZoneMinValue = p.deadZoneMinValue;
			deadZoneMaxValue = p.deadZoneMaxValue;
			return *this;
		}
		bool operator ==(const JoyAxisWrapper& p) const
		{
			return actionType == p.actionType &&
				axisIdentifier == p.axisIdentifier &&
				joyDesc == p.joyDesc &&
				currentValue == p.currentValue &&
				minValue == p.minValue &&
				maxValue == p.maxValue &&
				externalMinValue == p.externalMinValue &&
				externalMaxValue == p.externalMaxValue &&
				deadZoneMinValue == p.deadZoneMinValue &&
				deadZoneMaxValue == p.deadZoneMaxValue;
		}
	};
}
