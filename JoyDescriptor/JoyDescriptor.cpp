#include "JoyDescriptor/JoyDescriptor.h"


int JoyDescriptor::RetrieveModuleIndex()
{
    return moduleIndex;
}

std::string JoyDescriptor::FetchModuleName()
{
    return moduleName;
}

SDL_InputDevice* JoyDescriptor::AccessInputDevice()
{
    return inputDevice;
}

void JoyDescriptor::DefineModuleIndex(int id)
{
    moduleIndex = id;
}

void JoyDescriptor::DefineModuleName(std::string name)
{
    moduleName = name;
}

void JoyDescriptor::DefineInputDevice(SDL_InputDevice* device)
{
    inputDevice = device;
}

bool JoyDescriptor::isModuleEmpty()
{
    return moduleIndex == -1 || moduleName == "" || inputDevice == nullptr;
}

JoyDescriptor& JoyDescriptor::operator=(const JoyDescriptor& p)
{
    moduleIndex = p.moduleIndex;
    moduleName = p.moduleName;
    inputDevice = p.inputDevice;
    return *this;
}

bool JoyDescriptor::operator==(const JoyDescriptor& p) const
{
    return moduleIndex == p.moduleIndex &&
    moduleName == p.moduleName &&
    inputDevice == p.inputDevice;
}
