#pragma once
#include <string> 
#include "SDL/SDL.h"

#ifdef USERINPUT_EXPORTS
    #define USERINPUT_API __declspec(dllexport) 
#else
    #define USERINPUT_API __declspec(dllimport) 
#endif

class USERINPUT_API IJoyDescriptor
{
public:
	virtual int RetrieveModuleIndex() = 0;
	virtual std::string FetchModuleName() = 0;
	virtual SDL_Joystick* AccessInputDevice() = 0;

	virtual bool isModuleEmpty() = 0;

	virtual IJoyDescriptor& operator =(const IJoyDescriptor& p) ;
	virtual bool operator ==(const IJoyDescriptor& p) const ;
protected:
	int moduleIndex = -1;
	std::string moduleName = "";
	SDL_Joystick* inputDevice = nullptr;
};
