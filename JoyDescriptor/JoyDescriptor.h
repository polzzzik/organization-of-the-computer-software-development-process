#pragma once

#include <string> 
#include "SDL/SDL.h"
#include "JoyDescriptor/IJoyDescriptor.h"

class JoyDescriptor: public IJoyDescriptor
{
public:
	int RetrieveModuleIndex();
	std::string FetchModuleName();
	SDL_InputDevice* AccessInputDevice();

	void DefineModuleIndex(int id);
	void DefineModuleName(std::string name);
	void DefineInputDevice(SDL_InputDevice* device);
	bool isModuleEmpty();

	JoyDescriptor& operator =(const JoyDescriptor& p);
	bool operator == (const JoyDescriptor& p) const;
};
